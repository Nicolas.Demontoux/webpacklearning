```
// Exporter uniquement une fonction:
// dans /src/hello.js
const hello = () => {
    console.log('hello');
}

export { hello };

// importer une fonction 

import { hello } from "./hello"
```