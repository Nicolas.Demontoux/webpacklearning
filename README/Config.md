# webpack.config.js

Arborescence actuelle

![](https://i.imgur.com/Fg4WPfW.png)

npm install --save-dev html-webpack-plugin


## Config de base :
```
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

module.exports = {
  entry: './path/to/my/entry/file.js',
  mode: 'development',
  output: {
    filename: 'my-first-webpack.bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({ template: './src/index.html' }),
  ],
};
```

installer html-loader: 
```
npm install --save-dev html-loader
```

rajouter le module : 
```
module: {
    rules: [
      {
        test: /\.html$/i,
        loader: "html-loader",
        option: {
            minimize: true,
        }
      },
    ],
  },
```